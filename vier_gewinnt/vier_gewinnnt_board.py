from enum import Enum


class status(Enum):
    Empty=0
    Red=1
    Yellow=2

    def __str__(self):
        if self == self.Red:
            return 'ROT'
        elif self == self.Yellow:
            return 'GELB'
        else:
            return 'SCHROTT'


class board:
    def __init__(self):
        self.player= status.Red
        self.columns= []
        self.winner=0
        for i in range(7):
            self.columns.append([])
            for j in range(7):
                self.columns[i].append(status.Empty)

    def toggle(self):
        if self.player == status.Red:
            self.player= status.Yellow
        else:
            self.player=status.Red

    def throw(self, place):
        for i in range(7):
            if self.columns[place][i] is status.Empty:
                self.columns[place][i]= self.player
                self.lasty=i
                if self.check():
                    self.winner=1
                    self.toggle()
                    print(self.player," hat gewonnen!")
                    break
                else:
                    self.toggle()
                    break
               

    def check(self):
        return self.check_horizontal_vertikal() or self.check_diagonal()

    def check_horizontal_vertikal(self):
        for i in range(7):
            h_counter=0
            v_counter=0
            for j in range(7):
                if self.columns[i][j]==self.player:
                    h_counter=h_counter+1
                else:
                    h_counter=0
                if self.columns[j][i]==self.player:
                    v_counter=v_counter+1
                else:
                    v_counter=0
                if h_counter>=4 or v_counter>=4:
                    return True 
        return False

    def check_diagonal(self):
        for i in range(4):
            for j in range(4):
                a_counter=0
                d_counter=0
                for k in range(4):
                    if self.columns[i+k][j+k]==self.player:
                        a_counter=a_counter+1
                    else:
                        a_counter=0
                    if self.columns[6-(i+k)][j+k]==self.player:
                        d_counter=d_counter+1
                    else:
                        d_counter=0
                    if a_counter>=4 or d_counter>=4:
                        return True
        return False
            


            









                