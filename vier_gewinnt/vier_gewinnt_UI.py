from tkinter import *
from tkinter import messagebox
from vier_gewinnnt_board import *
from functools import partial


class UI:

    def __init__(self):
        self.window=Tk()
        self.window.title('Penelopes 4 gewinnt!')
        self.window.geometry('730x875')
        self.window.resizable(0,0)

        self.theboard=board()

        self.empty_image=PhotoImage(file='leer.png')
        self.gelb_image=PhotoImage(file='gelb.png')
        self.rot_image=PhotoImage(file='rot.png')
        self.werfen_image=PhotoImage(file='werfen.png')

        for i in range(7):
            werf_button=Button(master=self.window, image=self.werfen_image,command=partial(self.throwinto,i))
            werf_button.grid(row=0,column=i)

        self.columns=[]
        for i in range(7):
            self.columns.append([])
            for j in range(7):
                leeres_label=Label(master=self.window, image=self.empty_image, borderwidth=0)
                leeres_label.grid(row=i+1, column=j)
                self.columns[i].append(leeres_label)
        
        
        self.window.mainloop()
        

    def throwinto(self,place):
        self.theboard.throw(place)
        if self.theboard.player==status.Yellow:
            self.columns[6-self.theboard.lasty][place].configure(image=self.rot_image)
            self.window.update_idletasks()
            if self.theboard.winner==1:
                messagebox.showinfo(title='GLückwunsch!',message="ROT hat gewonnen")
        else:
            self.columns[6-self.theboard.lasty][place].configure(image=self.gelb_image)
            self.window.update_idletasks()
            if self.theboard.winner==1:
                messagebox.showinfo(title='GLückwunsch!',message="GELB hat gewonnen")
            
        
myui = UI()





